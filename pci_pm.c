// SPDX-License-Identifier: GPL

#include <linux/module.h>
#include <linux/pci.h>
#include <linux/pm_runtime.h>

static const struct dev_pm_ops pci_pm_ops = {};

static int pci_pm_probe(struct pci_dev *pdev, const struct pci_device_id *id)
{
	pdev->dev.driver->pm = &pci_pm_ops;
	pm_runtime_allow(&pdev->dev);
	pm_runtime_put(&pdev->dev);
	return 0;
}

static void pci_pm_remove(struct pci_dev *pdev)
{
	pm_runtime_get_noresume(&pdev->dev);
	pm_runtime_forbid(&pdev->dev);
}

static const struct pci_device_id pci_pm_table[] = {
	{ PCI_DRIVER_OVERRIDE_DEVICE_VFIO(PCI_ANY_ID, PCI_ANY_ID) },
	{}
};

MODULE_DEVICE_TABLE(pci, pci_pm_table);

static struct pci_driver pci_pm_driver = {
        .name                   = "pci_pm",
        .id_table               = pci_pm_table,
        .probe                  = pci_pm_probe,
        .remove                 = pci_pm_remove,
};

module_pci_driver(pci_pm_driver);
MODULE_LICENSE("GPL");
