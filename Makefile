ifeq ($(KERNEL_SRC),)
	KERNEL_SRC ?= /lib/modules/$(shell uname -r)/build
endif

SRC := $(shell pwd)

obj-m += pci_pm.o

all:
	$(MAKE) -C $(KERNEL_SRC) M=$(SRC) modules

modules_install:
	$(MAKE) -C $(KERNEL_SRC) M=$(SRC) modules_install

clean:
	$(MAKE) -C $(KERNEL_SRC) M=$(SRC) clean
